#ifndef V1
#define V1

#include <iostream>
#include <queue>
#include <stack>


// элемент бинарного дерева
template <typename VType>
struct Node {
    VType value; ; //значение, которое хранит класс
    Node *parent; // указатель на родительский узел
    Node *left; // указатель на левый элемент бинарного дерева
    Node *right; // указатель на правый элемент бинарного дерева
    // конструктор подставляет значения

    Node(VType value_, Node *parent_, Node *left_ = nullptr, Node *right_ = nullptr): value(value_), parent(parent_), left(left_), right(right_)  {}

    void show(std::string str) const
    {
        if (this->parent == nullptr)
            std::cout << "root<" << this->value << ">" << std:: endl;
        else
            std::cout << "<" << this->value << ">" << std:: endl;

        if (this->left != nullptr) {
            std::cout << str << "|" << std::endl;
            std::cout << str << "+-L";
            this->left->show(str + "| ");
        }

        if (this->right != nullptr) {
            std::cout << str << "|" << std::endl;
            std::cout << str << "+-R";
            this->right->show(str + "  ");
        }
    }

};

template <typename T>
class Tbtree;

//обход в прямом порядке
template <typename Type>
struct PreIterator {
    explicit PreIterator(): current(nullptr), end_ptr(nullptr) {}
    PreIterator<Type> begin(const Tbtree<Type> &tree){
        if (tree.root == nullptr)
            return end();

        current = tree.root;
        node_stack.push(nullptr);
        return *this;
    }
    PreIterator<Type> end()
    {
        return *this;
    }
    Type &operator*() const
    {
        return static_cast<Node<Type>*>(current)->value;
    }

    bool operator!=(const PreIterator<Type> &other){ // Проверка на неравенство

        return current != other.end_ptr;
    }

    bool operator==(const PreIterator<Type> &other)// Проверка на равенство
    {
        return current == other.end_ptr;
    }
    // Переход к следующему элементу (префиксный)
    Node<Type> *&operator++()
    {
        if (current != nullptr) {
            if (current->right != nullptr) {
                //если правое поддерево существуетс то поместить указатель на правый узел в стек
                node_stack.push(current->right);
            }

            if (current->left != nullptr) {
                current = current->left;
            } else {
                // подняться на один уровень если нет правого поддерева
                current = node_stack.top();
                node_stack.pop();
            }
        }

        return this->current;
    }

private:
    std::stack<Node<Type>* > node_stack;
    Node<Type> *current;
    Node<Type> *end_ptr;
};

//Симметричный обход
template <typename Type>
struct InIterator {
    explicit InIterator(): current(nullptr), end_ptr(nullptr) {}
    InIterator<Type> begin(const Tbtree<Type> &tree)
    {
        if (tree.root == nullptr)
            return end();

        current = tree.root;
        node_stack.push(nullptr);

        while (current->left != nullptr) {
            node_stack.push(current);
            current = current->left;
        }

        return *this;
    }
    InIterator<Type> end()
    {
        return *this;
    }
    Type &operator*() const
    {
        return static_cast<Node<Type>*>(current)->value;
    }

    bool operator!=(const InIterator<Type> &other) // Проверка на неравенство
    {
        return current != other.end_ptr;
    }

    bool operator==(const InIterator<Type> &other) // Проверка на равенство
    {
        return current == other.end_ptr;
    }

    Node<Type> *&operator++()  // Переход к следующему элементу (префиксный)
    {
        if (current != nullptr) {
            if (current->right != nullptr) {
                current = current->right;

                while (current->left != nullptr) {
                    node_stack.push(current);
                    current = current->left;
                }
            } else {
                current = node_stack.top();
                node_stack.pop();
            }
        }

        return this->current;
    }

private:
    std::stack<Node<Type>* > node_stack;
    Node<Type> *current;
    Node<Type> *end_ptr;
};

//Обход в обратном порядке
template <typename Type>
struct PostIterator {
    explicit PostIterator(): current(nullptr), end_ptr(nullptr) {}
    PostIterator<Type> begin(const Tbtree<Type> &tree)
    {
        if (tree.root == nullptr)
            return end();

        current = tree.root;
        node_stack.push(nullptr);

        while (current->left != nullptr || current->right != nullptr) {
            node_stack.push(current);

            if (current->left != nullptr) {
                current = current->left;
            } else {
                current = current->right;
            }
        }

        return *this;
    }
    PostIterator<Type> end()
    {
        return *this;
    }
    Type &operator*() const
    {
        return static_cast<Node<Type>*>(current)->value;
    }

    bool operator!=(const PostIterator<Type> &other)// Проверка на неравенство
    {
        return current != other.end_ptr;
    }

    bool operator==(const PostIterator<Type> &other)// Проверка на равенство
    {
        return current == other.end_ptr;
    }

    Node<Type> *&operator++()// переход к следующему элементу (префиксный)
    {
        if (current != nullptr) {
            Node<Type> *l_parent = node_stack.top();

            if (l_parent != nullptr && // текущий узел не корень дерева
                    current == l_parent->left && //левое поддерево следующего узла в стеке
                    l_parent->right != nullptr) { //у следующего узла в стеке есть правое поддерево
                // надо обработать правое поддерево следующего узла в стеке
                current = l_parent->right;

                // перейти к низшему узлу левого поддерева
                // если нет левого поддерева, то перейти к низшему узлу правого поддерева
                while (current->left != nullptr || current->right != nullptr) {
                    node_stack.push(current);

                    if (current->left != nullptr) {
                        current = current->left;
                    } else {
                        current = current->right;
                    }
                }
            } else {
                // подняться на один уровень
                current = node_stack.top();
                node_stack.pop();
            }
        }

        return this->current;
    }

private:
    std::stack<Node<Type>* > node_stack;
    Node<Type> *current;
    Node<Type> *end_ptr;
};

//Обход в ширину
template <typename Type>
struct BroadIterator {
    explicit BroadIterator(): current(nullptr), end_ptr(nullptr) {}
    BroadIterator<Type> begin(const Tbtree<Type> &tree)
    {
        if (tree.root == nullptr)
            return end();

        current = tree.root;

        if (current->left != nullptr) {
            node_queue.push(current->left);
        }

        if (current->right != nullptr) {
            node_queue.push(current->right);
        }

        return *this;
    }
    BroadIterator<Type> end()
    {
        return *this;
    }
    Type &operator*() const
    {
        return static_cast<Node<Type>*>(current)->value;
    }

    bool operator!=(const BroadIterator<Type> &other) // Проверка на равенство
    {
        return current != other.end_ptr;
    }

    bool operator==(const BroadIterator<Type> &other)  // Проверка на равенство
    {
        return current == other.end_ptr;
    }

    Node<Type> *&operator++()// переход к следующему элементу (префиксный)
    {
        if (current != nullptr) {
            if (!node_queue.empty()) {
                current = node_queue.front();
                node_queue.pop();

                if (current->left != nullptr) {
                    node_queue.push(current->left);
                }

                if (current->right != nullptr) {
                    node_queue.push(current->right);
                }
            } else {
                // Очередь пустая-узлов больше нет
                current = nullptr;
            }
        }

        return this->current;
    }

private:
    std::queue<Node<Type>* > node_queue;
    Node<Type> *current;
    Node<Type> *end_ptr;
};

//************************************************************************************************************
// шаблон бинарного дерева
template <typename T>
class Tbtree
{
private:
    Node<T> *findNode(const T &value, Node<T> *ptr);
    void add_node(const T value, Node<T> *&ptr);
    void print_node(Node<T> *ptr);
    void remove(Node<T> *&ptr);
    void copy(Node<T> *&target, const Node<T> *source);


    Node<T> *root;//Указатель на корневой узел бинарного дерева
    bool (*less)(const T &, const T &);//Предикат сравнения "меньше"

public:

    explicit Tbtree(bool(*less_)(const T &, const T &) = [](const T &x, const T &y)// конструктор по-умолчанию
    {
        return x < y;
    }): root(nullptr), less(less_) {}

    Tbtree(const Tbtree<T> &other);//конструктор копирования
    Tbtree &operator=(const Tbtree<T> &other);//оператор присваивания
    ~Tbtree()
    {
        remove_tree();
    }
    void set_func(bool(*less_)(const T &, const T &))
    {
        less = less_;
    }
    bool search(const T &value)
    {
        return findNode(value, root) != nullptr;
    }
    void add_node(const T value)
    {
        add_node(value, root);
    }

    void remove_tree()
    {
        remove(root);
    }
    void remove_node(const T value);
    void print_nodes()
    {
        print_node(root);
    }
    void show()
    {
        if (root != nullptr)
            root->show("");
    }
    friend class PreIterator<T>;
    friend class InIterator<T>;
    friend class PostIterator<T>;
    friend class BroadIterator<T>;
};
//************************************************************************************************************
template <typename T>
inline Node<T> *Tbtree<T>::findNode(const T &value, Node<T> *ptr)
{
    if (ptr == nullptr)
        return nullptr;
    //  else if (ptr->value == value)
    else if (!less(value, ptr->value) && !less(ptr->value, value))
        return ptr;
    //  else if (ptr->value > value)
    else if (less(value, ptr->value))
        return findNode(value, ptr->left);
    else
        return findNode(value, ptr->right);
}

template <typename T>
inline void Tbtree<T>::add_node(const T value, Node<T> *&ptr)
{
    if (ptr == nullptr) {
        ptr = new Node<T>(value, ptr);
        ptr->value = value;
        //  } else if (value < ptr->value) {
    } else if (less(value, ptr->value)) {
        if (ptr->left != nullptr)
            add_node(value, ptr->left);
        else {
            ptr->left = new Node<T>(value, ptr);
            ptr->left->value = value;
        }

        //  } else if (value > ptr->value) {
    } else if (less(ptr->value, value)) {
        if (ptr->right != nullptr)
            add_node(value, ptr->right);
        else {
            ptr->right = new Node<T>(value, ptr);
            ptr->right->value = value;
        }
    }
}

template <typename T>
inline void Tbtree<T>::print_node(Node<T> *ptr)
{
    //  InOrder
    if (ptr) {
        print_node(ptr->left);
        std::cout <<  ptr->value <<  std::endl;
        print_node(ptr->right);
    }//if
}

template <typename T>
inline void Tbtree<T>::remove(Node<T> *&ptr)
{
    //  PostOrder
    if (ptr != nullptr) {
        remove(ptr->left);
        remove(ptr->right);
        delete ptr;
        ptr = nullptr;
    }
}

template <typename T>
inline void Tbtree<T>::copy(Node<T> *&target, const Node<T> *source)
{
    //  PreOrder - позволяет сохранить существующую структуру дерева
    if (source != nullptr) {
        add_node(source->value, target);
        copy(target, source->left);
        copy(target, source->right);
    }
}


template <typename T>//Конструктор копирования
inline Tbtree<T>::Tbtree(const Tbtree<T> &other): root(nullptr)
{
    //Освободить существующие ресурсы
    //remove(root);
    //Присвоить создаваемому объекту данные-члены класса из исходного объекта
    less = other.less;
    copy(root, other.root);
}

//Оператор присваивания
template <typename T>
inline Tbtree<T> &Tbtree<T>::operator=(const Tbtree &other)
{
    if (this != &other) {
        //Освободить существующие ресурсы
        remove(root);
        //Присвоить создаваемому объекту данные-члены класса из исходного объекта
        less = other.less;
        copy(root, other.root);
    }

    return *this;
}

//1 - Удаляемый узел не имеет потомков, тогда его можно просто удалить.
//2 - Удаляемый узел имеет одного потомка, тогда на его место надо поставить потомка.
//3 - У удаляемого узла два потомка, в таком случае на место удаляемого узла необходимо
//    поставить минимум из правого поддерева. То есть один раз пройти вправо, и до конца влево
//    Если нет минимума из правого поддерева, то взять значение и правую ссылку из правого узла
//    и перенести их в удаляемый узел,изменить ссылку на родительский узел в узле,следующим за
//    правым узлом, а правый узел удалить.
template <typename T>
inline void Tbtree<T>::remove_node(const T value)
{
    Node<T> *ptr = root;
    Node<T> *parnt  = nullptr;

    while (ptr != nullptr && ptr->value != value) {
        parnt = ptr;

        //    if (value < ptr->value)
        if (less(value, ptr->value))
            ptr = ptr->left;
        else
            ptr = ptr->right;
    }

    if (ptr != nullptr) {
        Node<T> *removed = nullptr;

        if (ptr->left == nullptr || ptr->right == nullptr) {
            Node<T> *child = nullptr;
            removed = ptr;

            if (ptr->left != nullptr)
                child = ptr->left;
            else if (ptr->right != nullptr)
                child = ptr->right;

            if (parnt == nullptr)
                root = child;
            else {
                if (parnt->left == ptr)
                    parnt->left = child;
                else
                    parnt->right = child;
            }
        } else {
            Node<T> *most_left = ptr->right;
            Node<T> *most_left_parent = ptr;

            while (most_left->left != nullptr) {
                most_left_parent = most_left;
                most_left = most_left->left;
            }

            ptr->value = most_left->value;
            removed = most_left;

            //Если есть левый лист у правого узла
            if (most_left_parent->left == most_left) {
                if (most_left->right == nullptr)
                    most_left_parent->left = nullptr;
                else {
                    most_left_parent->left = most_left->right;
                    most_left->right->parent=most_left_parent;
                }

                // Если правый узел - лист
            } else if (most_left->right == nullptr) {
                most_left_parent->right = nullptr;
                //Если нет левого листа у правого узла
            } else {
                most_left_parent->right = most_left->right;
                most_left_parent->right->parent = most_left_parent;
            }
        }

        delete removed;
    }
}



#endif // V1

