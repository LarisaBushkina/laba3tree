#include "v1.h"

template <typename T>
void set_intersection(Tbtree<T> &set1, const Tbtree<T> &set2, Tbtree<T> &result_set)
{
    InIterator<T> it;
    result_set.remove_tree();

    for (it.begin(set2); it != it.end(); ++it)
        if (set1.search(*it))
            result_set.add_node(*it);
}

template <typename T> //Объединение множеств
void set_union(const Tbtree<T> &set1, const Tbtree<T> &set2, Tbtree<T> &result_set)
{
    result_set.remove_tree();
    result_set = set1;
    InIterator<T> it;

    for (it.begin(set2); it != it.end(); ++it)
        result_set.add_node(*it);
}

template <typename T> //Вычитание множеств
void set_difference(const Tbtree<T> &set1, Tbtree<T> &set2, Tbtree<T> &result_set)
{
    InIterator<T> it;
    result_set.remove_tree();

    for (it.begin(set1); it != it.end(); ++it)
        if (!set2.search(*it))
            result_set.add_node(*it);
}

void test()
{
    std::vector<int> v = {2, 4, 3, 1, 7, 5, 6, 8};
    Tbtree<int> s;

    for (int x : v)
        s.add_node(x);

    s.show();
    std::cout << "\n\nAfter remove: \n\n";
    s.remove_node(4);
    s.show();
}


int main()
{
    setlocale(LC_ALL, "RUS");
    //////////////////////
    test();
    return 0;
    //////////////////////
    std::string phrase = "Каждый охотник желает знать,где сидит фазан";
    std::cout << "Фраза для анализа : " << std::endl;
    std::cout << phrase << std::endl;
    Tbtree<char> vow_set, con_set;
    char vowels[] = {'а', 'я', 'у', 'ю', 'э', 'е', 'ы', 'и', 'о', 'ё'};
    char consonants[] = {'б', 'п', 'в', 'ф', 'г', 'к', 'д', 'т', 'ж', 'ш', 'з', 'с', 'й', 'л', 'р', 'м', 'н', 'х', 'ч', 'щ', 'ъ', 'ь', 'ц'};
    int vow_cnt = 0;
    int con_cnt = 0;

    for (const auto &elm : vowels)
        vow_set.add_node(elm);

    for (const auto &elm : consonants)
        con_set.add_node(elm);

    for (const auto &ch : phrase)
        if (vow_set.search(tolower(ch)))
            vow_cnt++;
        else if (con_set.search(tolower(ch)))
            con_cnt++;

    std::cout << "Количество гласных = " << vow_cnt << std::endl;
    std::cout << "Количество согласных = " << con_cnt << std::endl;
    int val[] = {1, 2, 3};
    int val1[] = {1, 4, 5, 6};
    Tbtree<int> set1, set2, res_set;

    for (const auto &elm : val)
        set1.add_node(elm);

    std::cout << "Множество set1  " << std::endl;
    set1.print_nodes();

    for (const auto &elm : val1)
        set2.add_node(elm);

    std::cout << "Множество set2 " << std::endl;
    set2.print_nodes();
    set_union(set1, set2, res_set);
    std::cout << "Результат объединения множеств set1 и set 2" << std::endl;
    res_set.print_nodes();
    set_difference(set1, set2, res_set);
    std::cout << "Результат вычитания множества set2 из множества set 1" << std::endl;
    res_set.print_nodes();
    set_intersection(set1, set2, res_set);
    std::cout << "Результат пересечения множеств set1 и set 2" << std::endl;
    res_set.print_nodes();
    return 0;
}


